window.onload = function() {
  refreshState();
  var form = document.getElementById("upload_form");
  form.addEventListener('submit', handleForm);
};

window.setInterval(function(){
  refreshState();
}, 5000);

const url = 'https://teamnet-rd.ml/voicebot-api-admin'

function handleForm(event) { event.preventDefault(); }

function ajaxCall(urlParam, methode, successFunc, errorFunc, data=undefined, encoding) {
  $.ajax({
      url: urlParam,
      type: methode,
      data: data,
      processData: false, 
      contentType: false,
      cache: false,
      crossOrigin: 'false',
      success: successFunc,
      error: errorFunc
  })
}

errorFunc = function () {
    console.log("ERROR");
    alert('An error happened, please contact the support.');
}

function reloadChatbot() {
  const select = document.getElementById('ENV');
  console.log(select.value);
  successFunc = function (answer) {
      //console.log("reloadChatbot: SUCCESS");
      alert('Chatbot has been restarted (may take 20-30 seconds)');
      refreshState();
  }

  response = ajaxCall(url+'/api?env='+select.value, 'GET', successFunc, errorFunc);
}

function refreshState() {
  const select = document.getElementById('ENV');
  console.log(select.value);

  successFunc = function (answer) {
      //console.log("refreshState: SUCCESS");
      document.getElementById("state").innerHTML = answer;
      if(answer.includes("running")){
        document.getElementById("state").style.color="#66cc00";
      }
      else{
        document.getElementById("state").style.color="#e60000";
      }
      
  }
 
  errorFunc = function () {
      console.log("refreshState: ERROR");
      document.getElementById("state").innerHTML = "unknown";
      alert('An error happened, please contact the support.');
  }

  response = ajaxCall(url+'/state?env='+select.value, 'GET', successFunc, errorFunc);
}

function cloneDB() {
  response = ajaxCall(url+'/clone_DB', 'POST');
  document.querySelector('#cloneDB').disabled = true;
  alert('Lancement du script du clonage de BD avec SUCCES');
  setTimeout(() => {
    document.querySelector('#cloneDB').disabled = false;
  }, 60000);
}

function getListDictionnary() {
  const select = document.getElementById('ENV');
  console.log(select.value);

  successFunc = function (answer) {
    
    const words = answer.split('\n');
    // remove first element
    words.shift();
    // remove last element
    words.pop();
    // remove roles from every line
    words.forEach(element =>{
      var i = words.indexOf(element)
      words[i] =  element.substring(12)
    } );

    // show table of dictionnary on console
    console.table(words)
    // add br tag for skip line
    answer = words.join("<br>")
    // show list dictionnary on html element
    document.getElementById("listDictionnary").innerHTML = answer;
    
}

errorFunc = function () {
    document.getElementById("listDictionnary").innerHTML = "dictionnary not found";
    alert('An error happened, please contact the support.');
}
  response = ajaxCall(url+'/listDictionnary?env='+select.value, 'GET', successFunc, errorFunc);
}

function sendGraph() {
  const select = document.getElementById('ENV');
  console.log(select.value);
  var file = document.getElementById('fileInputGraph');

  successFunc = function (answer) {
    //console.log("refreshState: SUCCESS");
    alert('Updated graph.');
  }

  var formData = new FormData();

  formData.append("file", file.files[0], file.files[0].name);
  formData.append("upload_file", true);

  response = ajaxCall(url+'/graph?env='+select.value, 'POST', successFunc, errorFunc, formData, "multipart/form-data");
}

function sendConfig() {
  const select = document.getElementById('ENV');
  console.log(select.value);
  var file = document.getElementById('fileInputConfig');

  successFunc = function (answer) {
    //console.log("refreshState: SUCCESS");
    alert('Updated Config.');
  }

  var formData = new FormData();

  formData.append("file", file.files[0], file.files[0].name);

  response = ajaxCall(url+'/sendConfig?env='+select.value, 'POST', successFunc, errorFunc, formData, "multipart/form-data");
}

function sendDico() {
  const select = document.getElementById('ENV');
  console.log(select.value);
  var files = document.getElementById('fileInputDico');

  successFunc = function (answer) {
    //console.log("refreshState: SUCCESS");
    alert('Updated dictionnaries.');
  }

  var formData = new FormData();


  $.each(files.files, function(i, file) {
    formData.append("files", file, file.name);
    formData.append("upload_file", true);
  });

  response = ajaxCall(url+'/unzipDictionnary?env='+select.value, 'POST', successFunc, errorFunc, formData, "multipart/form-data");
}

function dumpDev() {
  //response = ajaxCall('http://teamnet-rd.ml:55000/dev', 'GET');
}

function dumpTest() {
  //response = ajaxCall('http://teamnet-rd.ml:55000/test', 'GET');
}

function dumpProd() {
  //response = ajaxCall('http://teamnet-rd.ml:55000/prod', 'GET');
}
